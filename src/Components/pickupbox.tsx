import React, { Component } from "react";
import PickupLine, { Line } from "./pickupline";

interface PickupLineProps {
  lines: Array<Line>;
}

interface PickupBoxState {}

class PickupBox extends Component<PickupLineProps, PickupBoxState> {
  render() {
    return (
      <div className="pickupbox">
        <h1> Pickup box with all pick up lines should go here </h1>
        {this.props.lines.map((line, index) => (
          <PickupLine line={line} key={index} />
        ))}
      </div>
    );
  }
}

export default PickupBox;
