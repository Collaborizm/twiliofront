import React, { Component } from "react";

interface Line {
  title: string;
}

interface PickupLineProps {
  line: Line;
}

interface PickupLineState {}

class PickupLine extends Component<PickupLineProps, PickupLineState> {
  render() {
    return <li className="lines">{this.props.line.title}</li>;
  }
}

export { PickupLine as default, Line };
