import React, { Component } from "react";
import PickupBox from "./Components/pickupbox";
import Nav from "./Components/navbar";
import axios from "axios";
import { Line } from "./Components/pickupline";

interface AppProps {}

interface AppState {
  lines: Array<Line>;
}

class App extends Component<AppProps, AppState> {
  constructor(props) {
    super(props);
    this.state = { lines: [] };
  }

  getLines() {
    axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then(res => {
        console.log(res, "--main?");
        this.setState({ lines: res.data });
      })
      .catch(err => console.log(err));
  }

  componentDidMount() {
    this.getLines();
  }

  render() {
    console.log(this.state.lines);

    return (
      <div>
        <Nav />
        <main className="container">
          <PickupBox lines={this.state.lines} />
        </main>
      </div>
    );
  }
}

export default App;
