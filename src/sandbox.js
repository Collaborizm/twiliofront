const axios = require("axios");

function getLines() {
  axios
    .get("https://jsonplaceholder.typicode.com/todos")
    .then(res => {
      console.log(res);
    })
    .catch(err => console.log(err));
}

getLines();
